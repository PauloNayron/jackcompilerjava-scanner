package br.ufma.ecp;

import static br.ufma.ecp.token.TokenType.*;

import br.ufma.ecp.token.Token;
import br.ufma.ecp.token.TokenType;

public class Parser {
    private Scanner scan;
    private Token currentToken;
    private Token peekToken;

    private StringBuilder xmlOutput = new StringBuilder();

    public Parser (byte[] input) {
        scan = new Scanner(input);
        nextToken();
    }

    private void nextToken() {
        currentToken = peekToken;
        peekToken = scan.nextToken();
    }

    void parser () {
        parseClass();
    }

    /**
     * 'class' className '{' classVarDec* subroutineDec* '}' <BR>
     * 'class' className '{' classVarDec '}'
     * */
    void parseClass() {
        printNonTerminal("class");
        expectPeek(CLASS);
        expectPeek(IDENTIFIER);
        expectPeek(LBRACE);
        while (peekToken.type == FIELD || peekToken.type == STATIC) {
            parseClassVarDec();
        }
        expectPeek(RBRACE);
        printNonTerminal("/class");
    }

    /**
     * //( 'static' | 'field' ) type varName ( ',' varName)* ';'
     * */
    public void parseClassVarDec() {
        printNonTerminal("classVarDec");
        expectPeek(FIELD, STATIC);
        expectPeek(INT, CHAR, BOOLEAN, IDENTIFIER);
        expectPeek(IDENTIFIER);
        while (peekToken.type == COMMA) {
            expectPeek(COMMA);
            expectPeek(IDENTIFIER);
        }
        expectPeek(SEMICOLON);
        printNonTerminal("/classVarDec");
    }

    /**
     * letStatement -> 'let' varName  '=' term ';'
     * term -> number;
     * */
    void parseLet() {
        printNonTerminal("letStatement");
        expectPeek(LET);
        expectPeek(IDENTIFIER);
        expectPeek(EQ);
        parseExpression();
        expectPeek(SEMICOLON);
        printNonTerminal("/letStatement");
    }

    void parseExpression () {
        printNonTerminal("expression");
        parseTerm();
        while (isOperator(peekToken.type)) {
            expectPeek(peekToken.type);
            parseTerm();
        }
        printNonTerminal("/expression");
    }

    /**
     * integerConstant | stringConstant | keywordConstant | varName |
     * varName '[' expression ']' | subroutineCall | '(' expression ')' | unaryOp term
     * */
    void parseTerm () {
        printNonTerminal("term");
        if (isTerm()) { // integerConstant | stringConstant | keywordConstant
            expectPeek(peekToken.type);
        } else if (peekTokenIs(IDENTIFIER)) {  // varName | varName '[' expression ']'
            expectPeek(IDENTIFIER);
            if (peekTokenIs(DOT)) {
                expectPeek(DOT);
                expectPeek(IDENTIFIER);
                expectPeek(LPAREN);
                this.parseExpressionList();
                expectPeek(RPAREN);
            }
        } else if (peekTokenIs(LPAREN)) { // '(' expression ')'
            expectPeek(LPAREN);
            this.parseExpression();
            expectPeek(RPAREN);
        } else if (isUnaryOp()) {
            expectPeek(peekToken.type);
            this.parseTerm();
        }
        printNonTerminal("/term");
    }


    // auxiliares
    boolean currentTokenIs (TokenType type) {
        return currentToken.type == type;
    }

    boolean peekTokenIs (TokenType type) {
        return peekToken.type == type;
    }
    private void expectPeek (TokenType type) {
        if (peekToken.type == type ) {
            nextToken();
            xmlOutput.append(String.format("%s\r\n", currentToken.toString()));
        } else {
            throw new Error("Syntax error - expected "+type+" found " + peekToken.lexeme);
        }
    }

    private void expectPeek(TokenType... types) {
        for (TokenType type : types) {
            if (peekToken.type == type) {
                expectPeek(type);
                return;
            }
        }
        throw new Error("Syntax error ");
    }

    public String XMLOutput() {
        return xmlOutput.toString();
    }

    private void printNonTerminal(String nterminal) {
        xmlOutput.append(String.format("<%s>\r\n", nterminal));
    }

    /**
     * 'if' '(' expression ')' '{' statements '}' ('else' '{' statements '}')?
     * */
    public void parseIf() {
        printNonTerminal("ifStatement");
        expectPeek(IF);
        expectPeek(LPAREN);
        this.parseExpression();
        expectPeek(RPAREN);
        expectPeek(LBRACE);
        this.parseStatements();
        expectPeek(RBRACE);
        printNonTerminal("/ifStatement");
    }

    /**
     * statement*
     * */
    private void parseStatements() {
        printNonTerminal("statements");
        while (this.isStatement()) {
            switch (peekToken.type) {
                case LET -> this.parseLet();
                case IF -> this.parseIf();
                case DO -> this.parseDo();
                case WHILE -> this.parseWhile();
                case RETURN -> this.parseReturn();
            }
        }
        printNonTerminal("/statements");
    }

    private void parseWhile() {
        printNonTerminal("whileStatement");
        expectPeek(WHILE);
        expectPeek(LPAREN);
        this.parseExpression();
        expectPeek(RPAREN);
        expectPeek(LBRACE);
        this.parseStatements();
        expectPeek(RBRACE);
        printNonTerminal("/whileStatement");
    }

    private void parseVar() {
        printNonTerminal("varDec");
        expectPeek(VAR);
        if (isType()) {
            expectPeek(peekToken.type);
        }
        expectPeek(IDENTIFIER);
        expectPeek(SEMICOLON);
        printNonTerminal("/varDec");
    }

    /**
     * 'return' expression? ';'
     * */
    private void parseReturn() {
        printNonTerminal("returnStatement");
        expectPeek(RETURN);
        if (!peekTokenIs(SEMICOLON)) {
            this.parseExpression();
        }
        expectPeek(SEMICOLON);
        printNonTerminal("/returnStatement");
    }

    public void parseDo() {
        printNonTerminal("doStatement");
        expectPeek(DO);
        this.parseSubroutineCall();
        expectPeek(SEMICOLON);
        printNonTerminal("/doStatement");
    }

    /**
     * subroutineName '(' expressionList ')' |
     * ( className | varName ) '.' subroutineName '(' expressionList ')'
     * */
    private void parseSubroutineCall() {
        if (peekTokenIs(IDENTIFIER)) {
            expectPeek(IDENTIFIER);
            if (peekTokenIs(DOT)) {
                expectPeek(DOT);
                expectPeek(IDENTIFIER);
                expectPeek(LPAREN);
                this.parseExpressionList();
                expectPeek(RPAREN);
            } else if (peekTokenIs(LPAREN)) {
                expectPeek(LPAREN);
                this.parseExpressionList();
                expectPeek(RPAREN);
            }
        }
    }

    /**
     * (expression (',' expression)* )?
     * */
    private void parseExpressionList() {
        printNonTerminal("expressionList");
        while (this.isTerm() || peekTokenIs(IDENTIFIER) || peekTokenIs(LPAREN)) {
            this.parseExpression();
            while (peekTokenIs(COMMA)) {
                expectPeek(COMMA);
                this.parseExpression();
            }
        }
        printNonTerminal("/expressionList");
    }

    /**
     * ('construtor'|'function'|'method') ('void'|type) subroutineName
     * '(' parameterList ')' subroutineBody
     * */
    public void parseSubroutineDec() {
        printNonTerminal("subroutineDec");
        if (this.isSubroutineDec()) { // ('construtor'|'function'|'method')
            expectPeek(peekToken.type);
        }
        if (this.isType()) {
            expectPeek(peekToken.type);
        }
        expectPeek(IDENTIFIER);
        expectPeek(LPAREN);
        this.parseParameterList();
        expectPeek(RPAREN);

        this.parseSubroutineBody();

        printNonTerminal("/subroutineDec");
    }

    /** subroutineBody
     * '{' varDec* statements '}'
     * **/
    private void parseSubroutineBody() {
        printNonTerminal("subroutineBody");
        expectPeek(LBRACE);
        while (peekTokenIs(VAR)) {
            this.parseVar();
        }
        this.parseStatements();
        expectPeek(RBRACE);
        printNonTerminal("/subroutineBody");
    }

    /**
     * ((type varName) (',' varName)*)?
     * */
    private void parseParameterList() {
        printNonTerminal("parameterList");
        while (isType()) {
            expectPeek(peekToken.type);
            expectPeek(IDENTIFIER);
            if (peekTokenIs(COMMA)) {
                expectPeek(COMMA);
            }
        }
        printNonTerminal("/parameterList");
    }

    private boolean isTerm() {
        return peekTokenIs(INT) || peekTokenIs(STRING) || peekTokenIs(NUMBER) || isKeywordConstant();
    }

    private boolean isStatement() {
        return peekTokenIs(LET) || peekTokenIs(IF) || peekTokenIs(WHILE) || peekTokenIs(DO) || peekTokenIs(RETURN);
    }

    private boolean isUnaryOp() {
        return peekTokenIs(MINUS) || peekTokenIs(NOT);
    }

    private boolean isOperator(TokenType type) {
        return type.ordinal() >= PLUS.ordinal() && type.ordinal() <= EQ.ordinal();
    }

    private boolean isVarDec() {
        return peekTokenIs(LET) || peekTokenIs(IF) || peekTokenIs(DO) || peekTokenIs(WHILE) || peekTokenIs(RETURN) ||
                peekTokenIs(FIELD) || peekTokenIs(VAR);
    }

    private boolean isKeywordConstant() {
        return peekTokenIs(TRUE) || peekTokenIs(FALSE) || peekTokenIs(NULL) || peekTokenIs(THIS);
    }

    private boolean isType() {
        return peekTokenIs(VOID) || peekTokenIs(INT) || peekTokenIs(CHAR) || peekTokenIs(BOOLEAN) || isVarName();
    }

    private boolean isVarName() {
        return peekTokenIs(IDENTIFIER);
    }

    private boolean isSubroutineDec() {
        return peekTokenIs(CONSTRUCTOR) || peekTokenIs(FUNCTION) || peekTokenIs(METHOD) || this.isVarName();
    }

    public void parse() {
        printNonTerminal("class");
        expectPeek(CLASS);
        expectPeek(IDENTIFIER);
        expectPeek(LBRACE);

        while (isVarDec()) this.parseClassVarDec();

        while (this.isSubroutineDec()) {
            this.parseSubroutineDec();
        }

        expectPeek(RBRACE);
        printNonTerminal("/class");
    }
}
